DK Compiler
==============================================
 
## Install

Exec command

```sh
$ npm install --save dk-compiler
```

## Docs

Make file **example.js** in dir **build** and run

```js
var dkCompiler = require('dk-compiler');

dkCompiler({
    appendToDir: {
        files: [
            ['./mainInDir.php', "index.php"],
        ]
    },
    path: "../",
    ignore: [
        ".git",
        ".idea",
        "docs/examples",
        "node_modules",
        "dist",
        "build",
        "*.xml",
        "*.json",
    ],
    isDistName: true,
    /* distName: "example", */
    archive: {
        name: "../dist/mod.zip",
    }
});
```

## About

### Notes

* This project adheres to [Semantic Versioning][SEM_VER]

### License

[MIT][MIT]

[MIT]: https://opensource.org/licenses/mit-license.php
[SEM_VER]: https://semver.org/
