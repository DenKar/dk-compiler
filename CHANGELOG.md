# Changelog

All notable changes to [README] project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.2.2 - 2018-08-23
- Generate declaration TS and fix list npm files

## 0.2.1 - 2018-08-22
### Rewrite
- to TypeScript language

## 0.2.0 - 2018-08-20
### Added
- Preprocessor constants
- Unpack archive by array data

## 0.1.0 - 2018-08-16
### Added
- Archiving selected folder
- Change folder when archiving
- Insert terse files into folders
- Ignore by: file, dir and extension

[README]: README.md