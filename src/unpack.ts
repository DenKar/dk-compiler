import * as  decompress from "decompress-archive";
import * as  path from "path";

declare var require: any;

export const dkc_unpack = function (archiveFile, files) {
    let dirName = path.dirname(require.main.filename) + "/";

    archiveFile = dirName + archiveFile;

    console.log("* Unpack:\n-----------------");

    for (var i = 0; i < files.length; i++) {
        let fileName = files[i];

        if (fileName) {
            decompress(archiveFile, fileName, (err) => {
                if (err) console.error(err)
            });

            console.log("=>", fileName);
        }
    }
};