"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var preprocessor_1 = require("./preprocessor");
exports.dkc_archive = function (opt) {
    var archiveOpt = {
        appendToDir: opt.appendToDir,
        name: opt.archive.name,
        distName: opt.distName,
        files: [],
        ignore: opt.ignore,
        path: opt.path,
        constants: opt.constants
    };
    makeArchive(archiveOpt);
};
function makeArchive(opt) {
    // require modules
    var archiver = require('archiver');
    var dirName = path.dirname(require.main.filename) + "/";
    var output = fs.createWriteStream(dirName + opt.name);
    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });
    output.on('end', function () {
        console.log('Data has been drained');
    });
    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            // log warning
        }
        else {
            // throw error
            throw err;
        }
    });
    archive.on('error', function (err) {
        throw err;
    });
    archive.pipe(output);
    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }
    opt.path = dirName + opt.path;
    console.log("* Add files:\n-----------------:");
    dirDt(opt.path, opt, archive, opt.distName);
    setTimeout(function () {
        archive.finalize();
    }, 1000);
}
function dirDt(path, opt, archive, distName) {
    fs.readdir(path, function (err, items) {
        for (var i = 0; i < items.length; i++) {
            var name_1 = items[i];
            var ext = name_1.match(/.+\.(\w+)$/i);
            var ignoreName = name_1;
            var curName = path + name_1;
            var curName2 = curName.replace(opt.path, "");
            var buffer = void 0;
            if (ext) {
                ignoreName = '*.' + ext[1];
                // preprocessor
                if (ext[1] === "php") {
                    buffer = preprocessor_1.dkc_preprocessor(fs, curName, opt.constants);
                }
            }
            if (opt.ignore.indexOf(ignoreName) >= 0
                || opt.ignore.indexOf(curName2) >= 0) {
                continue;
            }
            var newName = distName;
            if (name_1 !== "src") {
                newName += "/" + name_1;
            }
            console.log("=>", newName);
            if (buffer) {
                archive.append(buffer, { name: newName });
                continue;
            }
            else {
                archive.file(curName, { name: newName });
                if (isDir(curName)) {
                    dirDt(curName + "/", opt, archive, newName);
                }
            }
        }
        if (opt.appendToDir && opt.appendToDir.files) {
            fils(distName, opt.appendToDir, archive);
        }
    });
}
function isDir(path) {
    var ok = false;
    try {
        ok = fs.lstatSync(path).isDirectory();
    }
    catch (e) {
        // Handle error
        if (e.code == 'ENOENT') {
            //no such file or directory
            //do something
        }
        else {
            //do something else
        }
    }
    return ok;
}
function fils(path2, appendToDir, archive) {
    var dirName = path.dirname(require.main.filename) + "/";
    for (var i = 0; i < appendToDir.files.length; i++) {
        var item = appendToDir.files[i];
        var nm = item[0];
        var nme = dirName + nm;
        var nm2 = path2 + "/";
        if (item[1] !== "") {
            nm2 += item[1];
        }
        else {
            nm2 += path.basename(nm);
        }
        archive.file(nme, { name: nm2 });
    }
}
