"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dkc_preprocessor = function (fs, file, constants) {
    console.log("* Preprocessor :", file, "\n-----------------");
    var content = fs.readFileSync(file);
    var textChunk = content.toString('utf8');
    for (var key in constants) {
        var re = new RegExp("{{" + key + "}}", "g");
        textChunk = textChunk.replace(re, constants[key]);
    }
    var buffer = Buffer.from(textChunk);
    return buffer;
};
