import * as fs from "fs";
import * as  path from "path";
import {dkc_preprocessor} from "./preprocessor";

export const dkc_archive = (opt) => {

    let archiveOpt = {
        appendToDir: opt.appendToDir,
        name: opt.archive.name,
        distName: opt.distName,
        files: [],
        ignore: opt.ignore,
        path: opt.path,
        constants: opt.constants
    };

    makeArchive(archiveOpt);
};

function makeArchive(opt) {
    // require modules

    let archiver = require('archiver');

    let dirName = path.dirname(require.main.filename) + "/";

    let output = fs.createWriteStream(dirName + opt.name);
    let archive = archiver('zip', {
        zlib: {level: 9} // Sets the compression level.
    });

    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    output.on('end', function () {
        console.log('Data has been drained');
    });

    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            // log warning
        } else {
            // throw error
            throw err;
        }
    });

    archive.on('error', function (err) {
        throw err;
    });

    archive.pipe(output);

    if (typeof opt.files !== "object") {
        opt.files = [opt.files];
    }


    opt.path = dirName + opt.path;

    console.log("* Add files:\n-----------------:");

    dirDt(opt.path, opt, archive, opt.distName);

    setTimeout(function () {
        archive.finalize();
    }, 1000)

}

function dirDt(path, opt, archive, distName) {
    fs.readdir(path, function (err, items) {
        for (let i = 0; i < items.length; i++) {
            let name = items[i];
            let ext = name.match(/.+\.(\w+)$/i);
            let ignoreName = name;
            let curName = path + name;
            let curName2 = curName.replace(opt.path, "");
            let buffer;

            if (ext) {
                ignoreName = '*.' + ext[1];

                // preprocessor

                if (ext[1] === "php") {
                    buffer = dkc_preprocessor(fs, curName, opt.constants)
                }
            }

            if (opt.ignore.indexOf(ignoreName) >= 0
                || opt.ignore.indexOf(curName2) >= 0) {
                continue;
            }

            let newName = distName;

            if (name !== "src") {
                newName += "/" + name;
            }

            console.log("=>", newName);

            if (buffer) {
                archive.append(buffer, {name: newName});

                continue;
            } else {
                archive.file(curName, {name: newName});

                if (isDir(curName)) {
                    dirDt(curName + "/", opt, archive, newName);
                }
            }
        }


        if (opt.appendToDir && opt.appendToDir.files) {
            fils(distName, opt.appendToDir, archive)
        }
    });
}

function isDir(path) {
    let ok = false;

    try {
        ok = fs.lstatSync(path).isDirectory();
    } catch (e) {
        // Handle error
        if (e.code == 'ENOENT') {
            //no such file or directory
            //do something
        } else {
            //do something else
        }
    }

    return ok;
}

function fils(path2, appendToDir, archive) {
    let dirName = path.dirname(require.main.filename) + "/";

    for (let i = 0; i < appendToDir.files.length; i++) {
        let item = appendToDir.files[i];
        let nm = item[0];

        let nme = dirName + nm;

        let nm2 = path2 + "/";

        if (item[1] !== "") {
            nm2 += item[1];
        } else {
            nm2 += path.basename(nm);
        }

        archive.file(nme, {name: nm2});
    }
}