"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var archive_1 = require("./archive");
var info_1 = require("./info");
var info_2 = require("./info");
exports.dkc_info = info_2.dkc_info;
var unpack_1 = require("./unpack");
exports.dkc_run = function (opt) {
    opt.name = opt.name || info_1.dkc_info.name;
    if (typeof opt.appendToDir !== "object") {
        opt.appendToDir = {};
    }
    opt.appendToDir.files = opt.appendToDir.files || [];
    opt.appendToDir.ignore = opt.appendToDir.ignore || [];
    opt.unpack = opt.unpack || [];
    // archive
    if (typeof opt.archive !== "object") {
        opt.archive = {};
    }
    opt.constants = opt.constants || {};
    opt.ignore = opt.ignore || [];
    opt.path = opt.path || "./";
    opt.archive.files = opt.archive.files || [];
    opt.archive.name = opt.archive.name || "";
    if (!opt.distName && opt.isDistName) {
        opt.distName = info_1.dkc_info.name;
    }
    archive_1.dkc_archive(opt);
    setTimeout(function () {
        unpack_1.dkc_unpack(opt.archive.name, opt.unpack);
    }, 2000);
};
