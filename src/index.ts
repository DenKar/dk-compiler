import {dkc_archive} from "./archive";
import {dkc_info} from "./info";
export {dkc_info} from "./info";
import {dkc_unpack} from "./unpack";

export const dkc_run = function (opt) {
    opt.name = opt.name || dkc_info.name;

    if (typeof opt.appendToDir !== "object") {
        opt.appendToDir = {};
    }

    opt.appendToDir.files = opt.appendToDir.files || [];
    opt.appendToDir.ignore = opt.appendToDir.ignore || [];
    opt.unpack = opt.unpack || [];

    // archive

    if (typeof opt.archive !== "object") {
        opt.archive = {};
    }

    opt.constants = opt.constants || {};
    opt.ignore = opt.ignore || [];
    opt.path = opt.path || "./";
    opt.archive.files = opt.archive.files || [];
    opt.archive.name = opt.archive.name || "";

    if (!opt.distName && opt.isDistName) {
        opt.distName = dkc_info.name;
    }

    dkc_archive(opt);

    setTimeout(function () {
        dkc_unpack(opt.archive.name, opt.unpack);
    }, 2000);
};
