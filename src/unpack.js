"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var decompress = require("decompress-archive");
var path = require("path");
exports.dkc_unpack = function (archiveFile, files) {
    var dirName = path.dirname(require.main.filename) + "/";
    archiveFile = dirName + archiveFile;
    console.log("* Unpack:\n-----------------");
    for (var i = 0; i < files.length; i++) {
        var fileName = files[i];
        if (fileName) {
            decompress(archiveFile, fileName, function (err) {
                if (err)
                    console.error(err);
            });
            console.log("=>", fileName);
        }
    }
};
