export const dkc_preprocessor = (fs, file, constants) => {
    console.log("* Preprocessor :", file, "\n-----------------");

    let content = fs.readFileSync(file);
    let textChunk = content.toString('utf8');

    for (let key in constants) {
        let re = new RegExp("{{" + key + "}}", "g");

        textChunk = textChunk.replace(re, constants[key]);
    }

    let buffer = Buffer.from(textChunk);

    return buffer;
};
